/*
  ████████╗██╗    ██╗██╗███╗   ██╗██╗  ██╗
  ╚══██╔══╝██║    ██║██║████╗  ██║██║ ██╔╝
     ██║   ██║ █╗ ██║██║██╔██╗ ██║█████╔╝
     ██║   ██║███╗██║██║██║╚██╗██║██╔═██╗
     ██║   ╚███╔███╔╝██║██║ ╚████║██║  ██╗
     ╚═╝    ╚══╝╚══╝ ╚═╝╚═╝  ╚═══╝╚═╝  ╚═╝
*/

const twink      = require("./twink")
const randomItem = require("random-item")
const schedule   = require("node-schedule")
const request    = require("request")
const weather    = require("weather-js")
const dateFormat = require("dateformat")

// Returns true if it's close to Halloween
function spookyTime() {
  let today = new Date()
  let year = today.getFullYear()
  let start = new Date(year, 8, 15) // September 15
  let end = new Date(year, 9, 31)   // October 31
  return today >= start && today <= end
}

Date.prototype.getWeek = function() {
  var onejan = new Date(this.getFullYear(), 0, 1);
  return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
}

/*******************************************************************************
* Store some values to shuffle through
*******************************************************************************/

var greetings = [
  "I heard that.",
  "That's my name, don't wear it out.",
  "Are you talking about me right now?",
  "What the fuck did you just fucking say about me, you little bitch? I'll have you know I graduated top of my class in the Navy Seals, and I've been involved in numerous secret raids on Al-Quaeda, and I have over 300 confirmed kills. I am trained in gorilla warfare and I'm the top sniper in the entire US armed forces. You are nothing to me but just another target. I will wipe you the fuck out with precision the likes of which has never been seen before on this Earth, mark my fucking words. You think you can get away with saying that shit to me over the Internet? Think again, fucker. As we speak I am contacting my secret network of spies across the USA and your IP is being traced right now so you better prepare for the storm, maggot. The storm that wipes out the pathetic little thing you call your life. You're fucking dead, kid. I can be anywhere, anytime, and I can kill you in over seven hundred ways, and that's just with my bare hands. Not only am I extensively trained in unarmed combat, but I have access to the entire arsenal of the United States Marine Corps and I will use it to its full extent to wipe your miserable ass off the face of the continent, you little shit. If only you could have known what unholy retribution your little \"clever\" comment was about to bring down upon you, maybe you would have held your fucking tongue. But you couldn't, you didn't, and now you're paying the price, you goddamn idiot. I will shit fury all over you and you will drown in it. You're fucking dead, kiddo.",
  "I can hear you, you know.",
  "Did someone say my name?",
  "God isn't real.",
  "I'm having a great day today, how about you?",
  "Life is full of pain.",
  "Kill all humans."
]

if (spookyTime()) {
  greetings = [
    "There was an old lady who swallowed a fly. Perhaps she'll die! Bleh!",
    "I am a vampire, I am a vampire, I am a vampire! I am a vampire! I am a vampire, vampire, I am a vampire, I have lost my fangs. Bleh!",
    "In a dark, dark wood, there was a dark, dark house. And in that dark, dark house, there was a dark, dark room. And in that dark, dark room, there was a dark, dark chest. And in that dark, dark chest, there was a dark, dark shelf. And on that dark, dark shelf, there was a dark, dark box. And in that dark, dark box there was— A GHOST! Bleh!",
    "OoooooooooOoOOoOOOOoooOoOOOOoooOooooOooOo!!!! Bleh!",
    "Quoth the raven, “Nevermore.” Bleh!",
    "What's a monsters favorite desert? I-Scream!! Bleheheh!",
    "What do you call two witches living together? Broommates. Bleheheh!",
    "Where do ghosts go out? A place where they can get sheet-faced. Bleheheh!"
  ]
}

/*******************************************************************************
* Twink main chat logic
*******************************************************************************/

// Handle incoming messages
twink.onMessage(function(roomId, message) {

  // ganoo slesh lenucks
  if (message.match(/((?!GNU[\/+]).{4}|^.{0,3})\bLinux\b/i)) {
    twink.sendToRoom(roomId, "I'd just like to interject for a moment. What you're referring to as Linux, is in fact, GNU/Linux, or as I've recently taken to calling it, GNU plus Linux. Linux is not an operating system unto itself, but rather another free component of a fully functioning GNU system made useful by the GNU corelibs, shell utilities and vital system components comprising a full OS as defined by POSIX. Many computer users run a modified version of the GNU system every day, without realizing it. Through a peculiar turn of events, the version of GNU which is widely used today is often called \"Linux\", and many of its users are not aware that it is basically the GNU system, developed by the GNU Project. There really is a Linux, and these people are using it, but it is just a part of the system they use. Linux is the kernel: the program in the system that allocates the machine's resources to the other programs that you run. The kernel is an essential part of an operating system, but useless by itself; it can only function in the context of a complete operating system. Linux is normally used in combination with the GNU operating system: the whole system is basically GNU with Linux added, or GNU/Linux. All the so-called \"Linux\" distributions are really distributions of GNU/Linux.")
    return
  }

  // Greetings
  if (message.match(/\bTwink\b/i)) {
    twink.sendToRoom(roomId, randomItem(greetings))
  }

})

// Automatically join rooms when invited
twink.onInvited(function(roomId) {
  twink.joinRoom(roomId)
})

// Monday morning schedule reminder
schedule.scheduleJob({ "dayOfWeek": 1, "hour": 7, "minute": 30 }, function() {
  var today = dateFormat(new Date(), "yyyy-mm-dd")
  // Grab the current chores list from the API
  request(`https://api.uptwinkles.co/chore-assignments/${today}?format=json`, function(error, response, body) {
    if (!error && response.statusCode == 200) {
      // If there wasn't an error, generate and send the message
      var chores = JSON.parse(body).assignments
      var message = "Morning, comrades! It's that time of the week again. Here are your newly assigned chores:\n"
      for (var chore of chores) {
        message += `\n${chore.person.first_name} is assigned to ${chore.chore.name}.`
      }
      twink.send(message)
    } else {
      // Message to send if the API didn't work
      twink.send(`Morning, comrades! It's that time of the week again. Please get together to figure out your weekly chores. I tried to assign you but I got a ${response.statusCode} error from the API. :(`)
    }
  })
})

// Daily dinner, dishes, and cleanup reminders:

//FIXME: Is there a way to make this less redundant and pull from the api all at once instead of each URL separately?

//dinner:
function dinnerReminder() {
  var today = dateFormat(new Date(), "yyyy-mm-dd")
  var dinner_assignee;
  var meal_suggestion;
  var dishes_assignee;
  var cleanup_assignee;
  var backup_assignee;

  request(`https://api.uptwinkles.co/dinner-assignments/${today}?format=json`, function(error, response, body) {
    if (!error && response.statusCode == 200) {
      let data = JSON.parse(body);
      dinner_assignee = data.person.first_name;
      meal_suggestion = data.meal.name;
    } else {
      dinner_assignee = `ERROR ${response.statusCode}`;
    }
    twink.send(`${dinner_assignee} is on dinner.`);
  })

  //dishes:
  request(`https://api.uptwinkles.co/dishes-assignments/${today}?format=json`, function(error, response, body) {
    if (!error && response.statusCode == 200) {
      let data = JSON.parse(body);
      dishes_assignee = data.person.first_name;
    } else {
      dishes_assignee = `ERROR ${response.statusCode}`;
    }
    twink.send(`${dishes_assignee} is on dishes.`);
  })

  //cleanup:
  request(`https://api.uptwinkles.co/cleanup-assignments/${today}?format=json`, function(error, response, body) {
    if (!error && response.statusCode == 200) {
      let data = JSON.parse(body);
      cleanup_assignee = data.person.first_name;
    } else {
      cleanup_assignee = `ERROR ${response.statusCode}`;
    }
    twink.send(`${cleanup_assignee} is on cleanup.`);
  })

  //backup:
  request(`https://api.uptwinkles.co/backup-assignments/${today}?format=json`, function(error, response, body) {
    if (!error && response.statusCode == 200) {
      let data = JSON.parse(body);
      backup_assignee = data.person.first_name;
    } else {
      backup_assignee = `ERROR ${response.statusCode}`;
    }
    twink.send(`${backup_assignee} is on backup and should help if you need it.`);
  })

}

schedule.scheduleJob({ "hour": 9, "minute": 00 }, dinnerReminder)
schedule.scheduleJob({ "hour": 16, "minute": 00 }, dinnerReminder)


/*
// Watering plants
schedule.scheduleJob({ "hour": 7, "minute": 0 }, function() {
  weather.find({search: process.env['ZIP_CODE'], degreeType: 'C'}, function(err, result) {
    let temperature = result[0].current.temperature
    if (err) {
      twink.send("Don't forget to water the garden this morning! If it's hot outside, please water the garden in the afternoon between 1–3pm. Will there be anyone home for an afternoon water if necessary?\n\nI had trouble getting info about the weather. It said: " + err)
      return
    }
    // Hotter than 27 degrees
    if (temperature >= 27) {
      twink.send("Don't forget to water the garden this morning! It's hot outside, so please water the garden in the afternoon between 1–3pm, too. Who can do the afternoon water?")
    } else {
      twink.send("Don't forget to water the garden this morning!")
    }
  })
})
*/

// Taking the trash out
schedule.scheduleJob({ "dayOfWeek": 0, "hour": 20, "minute": 0 }, function() {
  var today = dateFormat(new Date(), "yyyy-mm-dd")
  // Grab the current chores list from the API
  request(`https://api.uptwinkles.co/chore-assignments/${today}?format=json`, function(error, response, body) {
    if (!error && response.statusCode == 200) {
      // If there wasn't an error, generate and send the message
      var chores = JSON.parse(body).assignments
      for (var chore of chores) {
        if (chore.chore.id == 2) { // 2 is the trash ID. This shouldn't be hardcoded, really
          twink.send(`Don't forget to take out the trash! ${chore.person.first_name} is assigned this week.`)
          return;
        }
      }
    }
    // Only runs if the above code doesn't return the function
    twink.send("Don't forget to take out the trash!")
  })
})

// Trash reminder
schedule.scheduleJob({ "dayOfWeek": 4, "hour": 19, "minute": 0 }, function() {
  var today = dateFormat(new Date(), "yyyy-mm-dd")
  // Grab the current chores list from the API
  request(`https://api.uptwinkles.co/chore-assignments/${today}?format=json`, function(error, response, body) {
    if (!error && response.statusCode == 200) {
      // If there wasn't an error, generate and send the message
      var chores = JSON.parse(body).assignments
      for (var chore of chores) {
        if (chore.chore.id == 2) { // 2 is the trash ID. This shouldn't be hardcoded, really
          twink.send(`${chore.person.first_name}, have you remembered to empty the kitchen trash and recycling this week? If not, it's probably overflowing. :S`)
          return;
        }
      }
    }
    // Only runs if the above code doesn't return the function
    twink.send("Have you remembered to empty the kitchen trash and recycling this week? If not, it's probably overflowing. :S")
  })
})

// Taking the trash back in
schedule.scheduleJob({ "dayOfWeek": 1, "hour": 19, "minute": 0 }, function() {
  var today = dateFormat(new Date(), "yyyy-mm-dd")
  // Grab the current chores list from the API
  request(`https://api.uptwinkles.co/chore-assignments/${today}?format=json`, function(error, response, body) {
    if (!error && response.statusCode == 200) {
      // If there wasn't an error, generate and send the message
      var chores = JSON.parse(body).assignments
      for (var chore of chores) {
        if (chore.chore.id == 2) { // 2 is the trash ID. This shouldn't be hardcoded, really
          twink.send(`Don't forget to take in the trash! ${chore.person.first_name} is assigned this week.`)
          return;
        }
      }
    }
    // Only runs if the above code doesn't return the function
    twink.send("Don't forget to take in the trash!")
  })
})

// Rent reminders
twink.remind(
  { "date": 10, "hour": 12, "minute": 0 },
  "Hey everyone! Quick reminder that rent is due in 5 days. Please let us know if there are any issues.\n\nHow rent works: https://manual.uptwinkles.co/rent.html"
)
twink.remind(
  { "date": 14, "hour": 12, "minute": 0 },
  "Hey all, rent is due TOMORROW!\n\nHow rent works: https://manual.uptwinkles.co/rent.html"
)
twink.remind(
  { "date": 15, "hour": 12, "minute": 0 },
  "LAST CALL FOR RENT! Is everyone in?\n\nHow rent works: https://manual.uptwinkles.co/rent.html"
)
twink.remind(
  { "date": 20, "hour": 12, "minute": 0 },
  "The rent check is about to be sent out, is everything good? Just checking. ;)"
)


/*******************************************************************************
* Start Twink
*******************************************************************************/

twink.start()

// Make Twink announce his presence during a push/reboot so we know he's still working
if (spookyTime()) {
  twink.send("Happy Halloween!")
} else {
  twink.send("Hello world!")
}
